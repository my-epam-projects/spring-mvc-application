package com.epam.esm.service;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.DatabaseErrorException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.service.impl.GiftCertificateServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.*;

class GiftCertificateServiceTest {


    @Mock
    private GiftCertificateRepository certificateRepository;
    @Mock
    private TagService tagService;
    @InjectMocks
    private GiftCertificateServiceImpl certificateService;
    @Mock
    private GiftCertificateTagService giftCertificateTagService;



    @BeforeEach
    public void setUp() {
        certificateRepository = mock(GiftCertificateRepository.class);
        tagService = mock(TagService.class);
        giftCertificateTagService = mock(GiftCertificateTagService.class);
        certificateService = new GiftCertificateServiceImpl(certificateRepository, tagService, giftCertificateTagService);
    }

    @AfterEach
    public void shutDown() {
        Mockito.reset(certificateRepository, tagService, giftCertificateTagService);
    }


    @Test
    void testGetAllSuccess() throws CustomNotFoundException {
        // Given
        GiftCertificate certificate1 = new GiftCertificate();
        certificate1.setId(1);
        certificate1.setName("Certificate1");
        certificate1.setDescription("Description1");
        certificate1.setPrice(1000);
        certificate1.setDuration(10);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());

        GiftCertificate certificate2 = new GiftCertificate();
        certificate1.setId(2);
        certificate1.setName("Certificate2");
        certificate1.setDescription("Description2");
        certificate1.setPrice(2000);
        certificate1.setDuration(30);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());
        List<GiftCertificate> mockCertificates = Arrays.asList(certificate1, certificate2);


        // Mock the behavior of the repository
        when(certificateRepository.getAll()).thenReturn(mockCertificates);

        // assigning tags into certificates
        when(tagService.getByGiftCertificate(anyInt()))
                .thenReturn(Arrays.asList(new TagDTO(1, "Tag1"), new TagDTO(2, "Tag2")));


        List<GiftCertificateDTO> result = certificateService.getAll();

        // checking
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(mockCertificates.size(), result.size());

        // Verify that repository.getAll() was called
        verify(certificateRepository, times(1)).getAll();

        // Verify that tagService.getByGiftCertificate() was called for each certificate
        for (GiftCertificate certificate : mockCertificates) {
            verify(tagService, times(1)).getByGiftCertificate(certificate.getId());
        }
    }

    @Test
    void testGetAllEmptyList() throws CustomNotFoundException {
        // Mocking the behavior of the repository to return an empty list
        when(certificateRepository.getAll()).thenReturn(Collections.emptyList());

        // When
        CustomNotFoundException exception = assertThrows(
                CustomNotFoundException.class,
                () -> certificateService.getAll()
        );

        // comparing error messages
        assertEquals("gift certificates are not found", exception.getMessage());

        // Verify that repository.getAll() was called
        verify(certificateRepository, times(1)).getAll();

        // Verify that tagService.getByGiftCertificate() was not called
        verify(tagService, never()).getByGiftCertificate(anyInt());
    }

    @Test
    void testGetById() throws CustomNotFoundException {
        // input certificate Id
        int certificateId = 1;

        GiftCertificate mockCertificate = new GiftCertificate();
        mockCertificate.setId(certificateId);
        mockCertificate.setName("Certificate1");
        mockCertificate.setDescription("Description1");
        mockCertificate.setPrice(1000);
        mockCertificate.setDuration(10);
        mockCertificate.setCreateDate(LocalDateTime.now());
        mockCertificate.setLastUpdateDate(LocalDateTime.now());

        // Mocking the behavior of the repository
        when(certificateRepository.getById(certificateId)).thenReturn(mockCertificate);

        // Mocking the behavior of the tagService
        when(tagService.getByGiftCertificate(certificateId))
                .thenReturn(Arrays.asList(new TagDTO(1, "Tag1"), new TagDTO(2, "Tag2")));


        GiftCertificateDTO result = certificateService.getById(certificateId);

//        checking
        assertNotNull(result);
        assertEquals(certificateId, result.getId());

        // Verify that repository.findById() was called
        verify(certificateRepository, times(1)).getById(certificateId);

        // Verify that tagService.getByGiftCertificate() was called
        verify(tagService, times(1)).getByGiftCertificate(certificateId);
    }

    @Test
    void testGetByIdCustomNotFoundException() throws CustomNotFoundException {
        // input certificate Id
        int certificateId = 1;

        // Mocking the behavior of the repository to return null
        when(certificateRepository.getById(certificateId)).thenReturn(null);

        // When
        CustomNotFoundException exception = assertThrows(
                CustomNotFoundException.class,
                () -> certificateService.getById(certificateId)
        );

        // comparing error messages
        assertEquals("Gift certificate with Id: " + certificateId + " is not found", exception.getMessage());

        // Verify that repository.findById() was called
        verify(certificateRepository, times(1)).getById(certificateId);

        // Verify that tagService.getByGiftCertificate() was not called
        verify(tagService, never()).getByGiftCertificate(anyInt());
    }

    @Test
    void testGetByTagName() throws CustomNotFoundException {
        // Given
        String tagName = "Tag1";
        Tag mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName(tagName);

        GiftCertificate certificate1 = new GiftCertificate();
        certificate1.setId(1);
        certificate1.setName("Certificate1");
        certificate1.setDescription("Description1");
        certificate1.setPrice(1000);
        certificate1.setDuration(10);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());

        GiftCertificate certificate2 = new GiftCertificate();
        certificate1.setId(2);
        certificate1.setName("Certificate2");
        certificate1.setDescription("Description2");
        certificate1.setPrice(2000);
        certificate1.setDuration(30);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());

        List<GiftCertificate> mockCertificates = Arrays.asList(certificate1, certificate2);


        // Mock the behavior of the tagService
        when(tagService.findByName(tagName)).thenReturn(mockTag);

        // Mock the behavior of the repository
        when(certificateRepository.getByTagId(mockTag.getId())).thenReturn(mockCertificates);


        List<GiftCertificateDTO> result = certificateService.getByTagName(tagName);

        // checking
        assertNotNull(result);
        assertFalse(result.isEmpty());

        // Verify that tagService.findByName() was called
        verify(tagService, times(1)).findByName(tagName);

        // Verify that repository.getByTagId() was called
        verify(certificateRepository, times(1)).getByTagId(mockTag.getId());
    }

    @Test
    void testGetByTagNameTagNotFoundException() {
        // Given non-existing tag name
        String tagName = "NonExistentTag";

        // Mock the behavior of the tagService to return null
        when(tagService.findByName(tagName)).thenReturn(null);


        CustomNotFoundException exception = assertThrows(
                CustomNotFoundException.class,
                () -> certificateService.getByTagName(tagName)
        );

        // comparing error messages
        assertEquals("tag name: NonExistentTag is not found", exception.getMessage());

        // Verify that tagService.findByName() was called
        verify(tagService, times(1)).findByName(tagName);

        // Verify that repository.getByTagId() was not called
        verify(certificateRepository, never()).getByTagId(anyInt());
    }

    @Test
    void testGetByTagNameEmptyCertificatesList() {
        // Valid tag Name
        String tagName = "ExistingTag";
        Tag mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName(tagName);

        // Mock the behavior of the tagService
        when(tagService.findByName(tagName)).thenReturn(mockTag);

        // Mock the behavior of the repository to return an empty list
        when(certificateRepository.getByTagId(mockTag.getId())).thenReturn(Collections.emptyList());


        CustomNotFoundException exception = assertThrows(
                CustomNotFoundException.class,
                () -> certificateService.getByTagName(tagName)
        );

        // comparing messages of exception
        assertEquals("gift certificates are not found for corresponding tag Name: " + tagName, exception.getMessage());

        // Verify that tagService.findByName() was called
        verify(tagService, times(1)).findByName(tagName);

        // Verify that repository.getByTagId() was called
        verify(certificateRepository, times(1)).getByTagId(mockTag.getId());
    }

    @Test
    void testGetByName() throws InValidInputException, CustomNotFoundException {
        // Giving same name into two gift certificate object
        String certificateName = "Certificate1";
        GiftCertificate certificate1 = new GiftCertificate();
        certificate1.setId(1);
        certificate1.setName(certificateName);
        certificate1.setDescription("Description1");
        certificate1.setPrice(1000);
        certificate1.setDuration(10);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());

        GiftCertificate certificate2 = new GiftCertificate();
        certificate1.setId(2);
        certificate1.setName(certificateName);
        certificate1.setDescription("Description2");
        certificate1.setPrice(2000);
        certificate1.setDuration(30);
        certificate1.setCreateDate(LocalDateTime.now());
        certificate1.setLastUpdateDate(LocalDateTime.now());

        List<GiftCertificate> mockCertificates = Arrays.asList(certificate1, certificate2);

        // Mock the behavior of the repository
        when(certificateRepository.findByPartialName(certificateName)).thenReturn(mockCertificates);


        List<GiftCertificateDTO> result = certificateService.getByName(certificateName);

        // checking
        assertNotNull(result);
        assertFalse(result.isEmpty());

        // Verify that repository.findByPartialName() was called
        verify(certificateRepository, times(1)).findByPartialName(certificateName);
    }

    @Test
    void testGetByNameEmptyCertificatesList() throws InValidInputException, CustomNotFoundException {
        // Giving non-existent name
        String certificateName = "NonExistentCertificate";

        // Mock the behavior of the repository to return an empty list
        when(certificateRepository.findByPartialName(certificateName)).thenReturn(Collections.emptyList());

//        handling thrown exception
        CustomNotFoundException exception = assertThrows(
                CustomNotFoundException.class,
                () -> certificateService.getByName(certificateName)
        );

        // checking messages
        assertEquals("Gift certificates are not found for given name: " + certificateName, exception.getMessage());

        // Verify that repository.findByPartialName() was called
        verify(certificateRepository, times(1)).findByPartialName(certificateName);
    }

    @Test
    void testGetByNameInvalidInputException() {

        // giving null value
        // handling exception
        InValidInputException exception = assertThrows(
                InValidInputException.class,
                () -> certificateService.getByName(null)
        );

        // comparing error messages
        assertEquals("input name is empty", exception.getMessage());

        // Verify that repository.findByPartialName() was not called
        verify(certificateRepository, never()).findByPartialName(anyString());
    }

    @Test
    void testCreateValidInputSuccess() throws InValidInputException, CustomNotFoundException, DatabaseErrorException {
        // request object
        GiftCertificateCreateDTO createDTO = new GiftCertificateCreateDTO(
                "Certificate1",
                "Description1",
                100,
                1000,
                "2023-08-29T06:12Z",
                "2023-08-29T06:12Z",
                Arrays.asList(1, 2) // Assuming valid tag IDs
        );

//        mocking findByIds method
        when(tagService.findByIds(createDTO.getTagIds())).thenReturn(true);

//        mock Certificate
        GiftCertificate mockCertificate = new GiftCertificate();
        mockCertificate.setId(1);
        mockCertificate.setName("Certificate1");
        mockCertificate.setDescription("Description1");
        mockCertificate.setPrice(100);
        mockCertificate.setDuration(1000);
        mockCertificate.setCreateDate(Utils.toLocalDateTime("2023-08-29T06:12Z"));
        mockCertificate.setLastUpdateDate(Utils.toLocalDateTime("2023-08-29T06:12Z"));

//        mocking save method
        when(certificateRepository.save(any())).thenReturn(mockCertificate);

//        mocking giftCertificate
        when(giftCertificateTagService.save(any())).thenReturn(true);

        // Act
        GiftCertificateDTO result = certificateService.create(createDTO);

        // Assert
        assertNotNull(result);
        assertEquals(mockCertificate.getId(), result.getId());

        // Verify that tagService.findByIds() was called
        verify(tagService, times(1)).findByIds(createDTO.getTagIds());

        // Verify that repository.save() was called
        verify(certificateRepository, times(1)).save(any());

        // Verify that certificateTagService.save() was called
        verify(giftCertificateTagService, times(1)).save(any());
    }

    @Test
    void testCreateInvalidInputThrowsInValidInputException() {

        // expected exception and invalid input
        InValidInputException exception = assertThrows(InValidInputException.class, () -> {
            certificateService.create(null);
        });

        // comparing messages
        assertEquals("request model is empty", exception.getMessage());

    }

    @Test
    void testCreateInvalidTagThrowsCustomNotFoundException() {
        // request object
        GiftCertificateCreateDTO createDTO = new GiftCertificateCreateDTO(
                "Certificate1",
                "Description1",
                100,
                1000,
                "2023-08-29T06:12Z",
                "2023-08-29T06:12Z",
                Arrays.asList(1, 2) //  invalid tag IDs
        );

        when(tagService.findByIds(createDTO.getTagIds())).thenReturn(false);

        //handling exception
        CustomNotFoundException exception = assertThrows(CustomNotFoundException.class, () -> {
            certificateService.create(createDTO);
        });

        assertEquals("Invalid tag input values", exception.getMessage());
    }


    @Test
    void testUpdate_ValidInput_Success() throws InValidInputException, CustomNotFoundException {
        // Arrange
        GiftCertificateUpdateDTO updateDTO = new GiftCertificateUpdateDTO(
                1,
                "Certificate1",
                "Description1",
                100,
                1000,
                "2023-08-29T06:12Z",
                "2023-08-29T06:12Z"
        );

        GiftCertificate certificate = GiftCertificateMapper.toEntity(updateDTO);
//        mocking update
        when(certificateRepository.update(any())).thenReturn(certificate);

        // Mock the behavior of tagService.getByGiftCertificate
        List<TagDTO> mockTagDTOList = List.of(new TagDTO(1, "Tag1"), new TagDTO(2, "Tag2"));
        when(tagService.getByGiftCertificate(anyInt())).thenReturn(mockTagDTOList);


        GiftCertificateDTO result = certificateService.update(updateDTO);

        // Assert
        assertNotNull(result);
        assertEquals(updateDTO.getId(), result.getId());

        // Verify that tagService.getByGiftCertificate was called once
        verify(tagService, times(1)).getByGiftCertificate(anyInt());
    }

    @Test
    void testUpdateInvalidInputThrowsInValidInputException() throws CustomNotFoundException {

        // handle exception and input null value
        assertThrows(InValidInputException.class, () -> {
            certificateService.update(null);
        });

        // Verify that repository.update and tagService.getByGiftCertificate were not called
        verify(certificateRepository, never()).update(any());
        verify(tagService, never()).getByGiftCertificate(anyInt());
    }

    @Test
    void testDeleteValidIdSuccess() throws CustomNotFoundException, DatabaseErrorException {
        // Valid gift certificate ID
        int certificateId = 1;
        GiftCertificate mockCertificate = new GiftCertificate();
        mockCertificate.setId(certificateId);
        mockCertificate.setName("Certificate1");
        mockCertificate.setDescription("Description1");
        mockCertificate.setPrice(100);
        mockCertificate.setDuration(1000);
        mockCertificate.setCreateDate(Utils.toLocalDateTime("2023-08-29T06:12Z"));
        mockCertificate.setLastUpdateDate(Utils.toLocalDateTime("2023-08-29T06:12Z"));

//        checking certificate value
        when(certificateRepository.getById(certificateId)).thenReturn(mockCertificate);

        // Mock repository.delete and giftCertificateTagService.deleteByCertificate
        when(giftCertificateTagService.deleteByCertificate(certificateId)).thenReturn(true);
        when(certificateService.delete(certificateId)).thenReturn(true);

        boolean result = certificateService.delete(certificateId);

        assertTrue(result);

        // Verify method calls
        verify(certificateRepository, times(1)).delete(certificateId);
    }

    @Test
    void testDeleteInvalidIdThrowsCustomNotFoundException() {
        // non existent certificate ID
        int certificateId = 1;

        // handle exception
        CustomNotFoundException exception = assertThrows(CustomNotFoundException.class, () -> {
            certificateService.delete(certificateId);
        });

        //comparing error messages
        assertEquals("Gift certificate with Id: " + certificateId + " is not found", exception.getMessage());

        // Verify that giftCertificateTagService.deleteByCertificate and repository.delete were not called
        verify(giftCertificateTagService, never()).deleteByCertificate(anyInt());
        verify(certificateRepository, never()).delete(anyInt());
    }


    @Test
    void testAddNewTagValidInputSuccess() throws InValidInputException, CustomNotFoundException, DatabaseErrorException {
        // Arrange
        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, List.of(1, 2));

        certificateService = Mockito.spy(certificateService);

        // skip validateAndHandleErrors and giftCertificateTagService.save
        doNothing().when(certificateService).validateAndHandleErrors(tagDTO);
        when(giftCertificateTagService.save(tagDTO)).thenReturn(true);


        boolean result = certificateService.addNewTag(tagDTO);

        // Assert
        assertTrue(result);

        // Verify method calls
        verify(certificateService, times(1)).validateAndHandleErrors(tagDTO);
        verify(giftCertificateTagService, times(1)).save(tagDTO);
    }

    @Test
    void testAddNewTagInvalidInputThrowsInValidInputException() throws CustomNotFoundException, InValidInputException {

        // handling expected exception and null input value
        InValidInputException exception = assertThrows(InValidInputException.class, () -> {
            certificateService.addNewTag(null);
        });
        // checking messages
        assertEquals("Request model cannot be null!!", exception.getMessage());

        // verify method was not called
        verify(giftCertificateTagService, never()).save(any());
    }

    @Test
    void testDeleteTagValidInputSuccess() throws CustomNotFoundException, InValidInputException, DatabaseErrorException {
        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, List.of(1, 2));

        certificateService = Mockito.spy(certificateService);
        // Mock the behavior of validateAndHandleErrors and giftCertificateTagService.delete
        doNothing().when(certificateService).validateAndHandleErrors(tagDTO);
        when(giftCertificateTagService.delete(tagDTO)).thenReturn(true);


        boolean result = certificateService.deleteTag(tagDTO);

        // Assertion
        assertTrue(result);

        // Verify method calls
        verify(certificateService, times(1)).validateAndHandleErrors(tagDTO);
        verify(giftCertificateTagService, times(1)).delete(tagDTO);
    }

    @Test
    void testDeleteTagInvalidInputThrowsInValidInputException() throws CustomNotFoundException, InValidInputException {
        // handle exception
        InValidInputException exception = assertThrows(InValidInputException.class, () -> {
            certificateService.deleteTag(null);
        });

        assertEquals("Request model cannot be null!!", exception.getMessage());

        // Verify giftCertificateTagService.delete were not called
        verify(giftCertificateTagService, never()).delete(any());
    }
}
