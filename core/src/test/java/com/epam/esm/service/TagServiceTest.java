package com.epam.esm.service;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.exceptions.NoDataException;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.impl.TagServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TagServiceTest {
    @Mock
    private TagRepository tagRepository;

    @InjectMocks
    private TagService tagService;

    @BeforeEach
    public void setUp() {
        tagRepository = mock(TagRepository.class);
        tagService = new TagServiceImpl(tagRepository);
    }

    @AfterEach
    public void shutDown() {
        Mockito.reset(tagRepository);
    }

    @Test
    void testGetAllTagsSuccess() throws CustomNotFoundException {
        Tag tag = new Tag();
        tag.setId(1);
        tag.setName("tag one");
        // Given
        List<Tag> mockTags = Collections.singletonList(tag);
        when(tagRepository.getAll()).thenReturn(mockTags);

        // getting
        List<TagDTO> result = tagService.getAll();

        // Then
        assertNotNull(result);
        assertFalse(result.isEmpty());
        assertEquals(mockTags.size(), result.size());
        assertEquals(mockTags.get(0).getName(), result.get(0).getName());

        // Verify that repository.getAll() was called
        verify(tagRepository, times(1)).getAll();
    }

    @Test
    void testGetAllTagsEmptyList() {
        // Given
        when(tagRepository.getAll()).thenReturn(Collections.emptyList());

        // getting customNotfound exception
        CustomNotFoundException exception = assertThrows(CustomNotFoundException.class, () -> tagService.getAll());

        // comparing messages
        assertEquals("Tags are not found", exception.getMessage());

        // Verifying repository.getAll() was called
        verify(tagRepository, times(1)).getAll();
    }


    @Test
    void testGetByIdSuccess() throws NoDataException, InValidInputException, CustomNotFoundException {
        // Input tag Id
        Integer tagId = 1;
        Tag mockTag = new Tag();
        mockTag.setId(tagId);
        mockTag.setName("tag one");
        when(tagRepository.findById(tagId)).thenReturn(mockTag);


        TagDTO result = tagService.getById(tagId);

        // assertions for results and comparing values
        assertNotNull(result);
        assertEquals(tagId, result.getId());
        assertEquals(mockTag.getName(), result.getName());

        // Verify that repository.findById() was called
        verify(tagRepository, times(1)).findById(tagId);
    }

    @Test
    void testGetByIdNoDataException() {
        // checking for null
        NoDataException exception = assertThrows(NoDataException.class, () -> tagService.getById(null));

        // comparing messages
        assertEquals("Tag id is required", exception.getMessage());
    }

    @Test
    void testGetByIdInValidInputException() {
        // checking with 0
        Integer tagId = 0;


        InValidInputException exception = assertThrows(InValidInputException.class, () -> tagService.getById(tagId));

        // comparing messages
        assertEquals("zero is not acceptable", exception.getMessage());
    }

    @Test
    void testGetByIdCustomNotFoundException() {
        // non-existing tag Id
        Integer tagId = 1;
        when(tagRepository.findById(tagId)).thenReturn(null);

        // assigning exception
        CustomNotFoundException exception = assertThrows(CustomNotFoundException.class, () -> tagService.getById(tagId));

        // comparing messages
        assertEquals("tag with Id: " + tagId + " is not found", exception.getMessage());

        // Verify that repository.findById() was called
        verify(tagRepository, times(1)).findById(tagId);
    }

    @Test
    void testCreateSuccess() throws NoDataException, AlreadyExistsException {
        // Given
        TagCreateDTO createDTO = new TagCreateDTO("NewTag");
        Tag mockTag = new Tag();
        mockTag.setId(1);
        mockTag.setName("NewTag");

//        checking tag name
        when(tagRepository.getByName(createDTO.getName())).thenReturn(null);
//        saving tag
        when(tagRepository.save(any(Tag.class))).thenReturn(mockTag);

        TagDTO result = tagService.create(createDTO);

        // checking and comparing values
        assertNotNull(result);
        assertEquals(mockTag.getId(), result.getId());
        assertEquals(createDTO.getName(), result.getName());

        // Verify that repository.getByName() and repository.save() were called
        verify(tagRepository, times(1)).getByName(createDTO.getName());
        verify(tagRepository, times(1)).save(any(Tag.class));
    }

    @Test
    void testCreateNoDataException() {
        // empty string value input
        TagCreateDTO createDTO = new TagCreateDTO("");


        NoDataException exception = assertThrows(NoDataException.class, () -> tagService.create(createDTO));

        // comparing error message
        assertEquals("Tag name cannot be empty", exception.getMessage());
    }

    @Test
    void testCreateAlreadyExistsException() {
        // giving existing inout tag name
        TagCreateDTO createDTO = new TagCreateDTO("ExistingTag");
        Tag existingTag = new Tag();
        existingTag.setId(1);
        existingTag.setName("ExistingTag");

        when(tagRepository.getByName(createDTO.getName())).thenReturn(existingTag);


        AlreadyExistsException exception = assertThrows(AlreadyExistsException.class, () -> tagService.create(createDTO));

        // comparing error messages
        assertEquals("Tag name " + createDTO.getName() + " is already exists", exception.getMessage());

        // Verify that repository.getByName() was called
        verify(tagRepository, times(1)).getByName(createDTO.getName());
    }

    @Test
    void testUpdateSuccess() throws NoDataException, AlreadyExistsException, InValidInputException {
        // giving valid input
        TagUpdateDTO updateDTO = new TagUpdateDTO(1, "UpdatedTag");
        Tag mockTag = new Tag();
        mockTag.setId(updateDTO.getId());
        mockTag.setName(updateDTO.getName());

        when(tagRepository.getByName(updateDTO.getName())).thenReturn(null);
        when(tagRepository.update(any(Tag.class))).thenReturn(mockTag);


        TagDTO result = tagService.update(updateDTO);

        // checking result values
        assertNotNull(result);
        assertEquals(mockTag.getId(), result.getId());
        assertEquals(updateDTO.getName(), result.getName());

        // Verify that repository.getByName() and repository.update() were called
        verify(tagRepository, times(1)).getByName(updateDTO.getName());
        verify(tagRepository, times(1)).update(any(Tag.class));
    }

    @Test
    void testUpdateNoDataException() {
        // Giving null value
        TagUpdateDTO updateDTO = new TagUpdateDTO(null, "UpdatedTag");


        NoDataException exception = assertThrows(NoDataException.class, () -> tagService.update(updateDTO));

        // comparing messages
        assertEquals("Tag id is required", exception.getMessage());
    }

    @Test
    void testUpdateEmptyNameException() {
        // Giving empty name
        TagUpdateDTO updateDTO = new TagUpdateDTO(1, "");


        NoDataException exception = assertThrows(NoDataException.class, () -> tagService.update(updateDTO));

        // comparing messages
        assertEquals("Tag name cannot be empty", exception.getMessage());
    }

    @Test
    void testUpdateAlreadyExistsException() {
        // Given
        TagUpdateDTO updateDTO = new TagUpdateDTO(1, "ExistingTag");
        Tag existingTag = new Tag();
        existingTag.setId(1);
        existingTag.setName("ExistingTag");
        when(tagRepository.getByName(updateDTO.getName())).thenReturn(existingTag);

        AlreadyExistsException exception = assertThrows(AlreadyExistsException.class, () -> tagService.update(updateDTO));

        // comparing error messages
        assertEquals("Tag name " + updateDTO.getName() + " is already exists", exception.getMessage());

        // Verify that repository.getByName() was called
        verify(tagRepository, times(1)).getByName(updateDTO.getName());
    }

    @Test
    void testDeleteSuccess() throws NoDataException, InValidInputException, CustomNotFoundException {
        // Giving valid input
        Integer tagId = 1;
        Tag mockTag = new Tag();
        mockTag.setId(tagId);
        mockTag.setName("Tag1");
        when(tagRepository.findById(tagId)).thenReturn(mockTag);
        when(tagRepository.delete(tagId)).thenReturn(true);


        Boolean result = tagService.delete(tagId);

        // Then
        assertTrue(result);

        // Verify that repository.findById() and repository.delete() were called
        verify(tagRepository, times(1)).findById(tagId);
        verify(tagRepository, times(1)).delete(tagId);
    }

    @Test
    void testDeleteNoDataException() {
        // Giving null value
        Integer tagId = null;

        NoDataException exception = assertThrows(NoDataException.class, () -> tagService.delete(tagId));

        // comparing error messages
        assertEquals("Tag id is required", exception.getMessage());
    }

    @Test
    void testDeleteInValidInputException() {
        // giving zero
        Integer tagId = 0;

        InValidInputException exception = assertThrows(InValidInputException.class, () -> tagService.delete(tagId));

        // comparing error messages
        assertEquals("zero is not acceptable", exception.getMessage());
    }

    @Test
    void testDeleteCustomNotFoundException() {
        // Giving non-existing tag ID
        Integer tagId = 1;
        when(tagRepository.findById(tagId)).thenReturn(null);

        CustomNotFoundException exception = assertThrows(CustomNotFoundException.class, () -> tagService.delete(tagId));

        // comparing messages
        assertEquals("tag with Id: " + tagId + " is not found", exception.getMessage());

        // Verify that repository.findById() was called
        verify(tagRepository, times(1)).findById(tagId);
    }
}
