package com.epam.esm.service;

import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.repository.GiftCertificateTagRepository;
import com.epam.esm.service.impl.GiftCertificateTagServiceImpl;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class GiftCertificateTagServiceTest {

    private GiftCertificateTagRepository repository;

    private GiftCertificateTagService service;

    @BeforeEach
    public void setUp() {
        repository = mock(GiftCertificateTagRepository.class);
        service = new GiftCertificateTagServiceImpl(repository);
    }

    @AfterEach
    public void shutDown() {
        Mockito.reset(repository);
    }

    @Test
    void testSaveValidInputSuccess() {
        // valid tagDto object
        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, Arrays.asList(2, 3, 4));

        // Mock the behavior of repository.save
        when(repository.save(any())).thenReturn(true);

        boolean result = service.save(tagDTO);

        // check result
        assertTrue(result);

        // Verify method calls
        verify(repository, times(tagDTO.getTagIds().size())).save(any());
    }

    @Test
    void testSaveSaveFailureReturnsFalse() {
        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, Arrays.asList(2, 3, 4));

        // Mock repository.save
        when(repository.save(any())).thenReturn(false);

        boolean result = service.save(tagDTO);

        assertFalse(result);

        // Verify method calls
        verify(repository, times(1)).save(any());
    }

    @Test
    void testDeleteValidInputSuccess() {

        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, Arrays.asList(2, 3, 4));

        // Mock repository.delete
        when(repository.delete(any())).thenReturn(true);

        boolean result = service.delete(tagDTO);

        // check result
        assertTrue(result);

        // Verify method calls
        verify(repository, times(tagDTO.getTagIds().size())).delete(any());
    }

    @Test
    void testDeleteDeleteFailureReturnsFalse() {

        GiftCertificateTagDTO tagDTO = new GiftCertificateTagDTO(1, Arrays.asList(2, 3, 4));

        // Mock  repository.delete
        when(repository.delete(any())).thenReturn(false);

        boolean result = service.delete(tagDTO);


        assertFalse(result);

        // Verify method calls
        verify(repository, times(1)).delete(any());
    }

    @Test
    void testDeleteByCertificateValidCertificateIdSuccess() {

        int certificateId = 1; //valid certificate ID

        // Mock repository.deleteByCertificate
        when(repository.deleteByCertificate(certificateId)).thenReturn(true);


        boolean result = service.deleteByCertificate(certificateId);

        assertTrue(result);


        // Verify method call
        verify(repository, times(1)).deleteByCertificate(certificateId);
    }

    @Test
    void testDeleteByCertificateDeleteFailureReturnsFalse() {
        // valid certificate ID
        int certificateId = 1;

        // Mock  repository.deleteByCertificate
        when(repository.deleteByCertificate(certificateId)).thenReturn(false);


        boolean result = service.deleteByCertificate(certificateId);

        assertFalse(result);


        // Verify method calls
        verify(repository, times(1)).deleteByCertificate(certificateId);
    }
}
