package com.epam.esm.repository;

import com.epam.esm.entity.Tag;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class TagRepositoryIntegrationTest {
    private TagRepository tagRepository;
    private EmbeddedDatabase database;
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        database = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .addScript("sql/schema.sql")
                .addScript("sql/tag-data.sql")
                .build();

        jdbcTemplate = new JdbcTemplate(database);
        tagRepository = new TagRepository(jdbcTemplate);
    }

    @Test
    void tesGetAllMethod() {
//        get all tags
        List<Tag> tagList = tagRepository.getAll();

//        check size
        assertNotNull(tagList);
        assertEquals(3, tagList.size());

//        check names
        assertEquals("tag one", tagList.get(0).getName());
        assertEquals("tag two", tagList.get(1).getName());
        assertEquals("tag three", tagList.get(2).getName());
    }

    @Test
    void testFindById() {
//        get tag
        Tag actualTag = tagRepository.findById(1);

        Tag expectedTag = new Tag();
        expectedTag.setId(1);
        expectedTag.setName("tag one");
//        check
        assertNotNull(actualTag);
        assertEquals(expectedTag.getName(), actualTag.getName());

    }

    @Test
    void testSaveMethod() {
        Tag tagToSave = new Tag();
        tagToSave.setName("NewTag");

//        save tag
        Tag savedTag = tagRepository.save(tagToSave);
//        check for null
        assertNotNull(savedTag);

        Tag expectedTag = tagRepository.findById(savedTag.getId());
        assertEquals(expectedTag.getName(), savedTag.getName());
    }

    @Test
    void testUpdate() {
        Tag existingTag = tagRepository.findById(1);
        assertNotNull(existingTag);

        // Modify the existing tag
        existingTag.setName("UpdatedTag");

//        update tag
        Tag updatedTag = tagRepository.update(existingTag);


        assertNotNull(updatedTag);
        assertEquals(existingTag.getId(), updatedTag.getId());
        assertEquals("UpdatedTag", updatedTag.getName());

        // Verify that the tag in the database is updated
        Tag retrievedTag = tagRepository.findById(existingTag.getId());
        assertNotNull(retrievedTag);
        assertEquals("UpdatedTag", retrievedTag.getName());

    }

    @Test
    void testDeleteMethod() {
        Assertions.assertTrue(tagRepository.delete(1));
        Assertions.assertFalse(tagRepository.delete(8));
    }

    @Test
    void testGetByNameMethod() {
//        get tag id 2
        Tag tag = tagRepository.getByName("tag two");
        assertNotNull(tag);
        assertEquals(2, tag.getId());

//        giving not existing data
        assertNull(tagRepository.getByName("tag four"));

    }

    @Test
    void testGetByCertificateMethod() {
        jdbcTemplate.execute("INSERT INTO gift_certificates( name, description, price, duration, create_date, last_update_date)\n" +
                "values ( 'certificate one', 'gives promotion for position', 100, 30, '2023-10-25 10:00:00',\n" +
                "         '2023-10-25 10:00:00')");
        jdbcTemplate.execute("INSERT INTO gift_certificate_tags(gift_certificate_id, tag_id)\n" +
                "VALUES (1, 2)");
        jdbcTemplate.execute("INSERT INTO gift_certificate_tags(gift_certificate_id, tag_id)\n" +
                "VALUES (1, 1)");

//        get tags by gift certificate Id
        List<Tag> tagList = tagRepository.getByCertificate(1);
//        check size
        assertEquals(2,tagList.size());

//      check tag names
        assertTrue(tagList.stream().anyMatch(tag -> tag.getName().equals("tag one")));
        assertTrue(tagList.stream().anyMatch(tag -> tag.getName().equals("tag two")));
    }

    @AfterEach
    public void tearDown() {
        database.shutdown();
    }
}
