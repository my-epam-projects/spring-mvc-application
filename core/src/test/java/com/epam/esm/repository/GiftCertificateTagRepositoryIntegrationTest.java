package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificateTags;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import static org.junit.jupiter.api.Assertions.assertTrue;

class GiftCertificateTagRepositoryIntegrationTest {

    private GiftCertificateTagRepository repository;
    private EmbeddedDatabase database;
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        database = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .addScript("sql/schema.sql")
                .addScript("sql/giftCertificate-data.sql")
                .addScript("sql/tag-data.sql")
                .addScript("sql/giftCertificateTag-data.sql")
                .build();

        jdbcTemplate = new JdbcTemplate(database);
        repository = new GiftCertificateTagRepository(jdbcTemplate);
    }

    @AfterEach
    public void tearDown() {
        database.shutdown();
    }
    @Test
    void testSaveMethod() {
        GiftCertificateTags certificateTags = new GiftCertificateTags(3, 1);

        boolean result = repository.save(certificateTags);
//        check save result
        assertTrue(result);
    }

    @Test
    void testDelete() {
        // Given
        Integer tagIdToDelete = 1;
        Integer certificateId = 1;
        GiftCertificateTags certificateTagsToDelete = new GiftCertificateTags(certificateId, tagIdToDelete);

        // When
        boolean deletionResult = repository.delete(certificateTagsToDelete);

        // Then
        assertTrue(deletionResult);

    }


    @Test
    void testDeleteByCertificate() {
        // Given
        Integer certificateIdToDelete = 1;

        // When
        boolean deletionResult = repository.deleteByCertificate(certificateIdToDelete);

        // Then
        assertTrue(deletionResult);
    }
}
