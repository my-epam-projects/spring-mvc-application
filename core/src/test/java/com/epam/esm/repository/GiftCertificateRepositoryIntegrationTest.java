package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificate;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class GiftCertificateRepositoryIntegrationTest {

    private GiftCertificateRepository repository;
    private EmbeddedDatabase database;
    private JdbcTemplate jdbcTemplate;

    @BeforeEach
    public void setUp() {
        database = new EmbeddedDatabaseBuilder().setType(EmbeddedDatabaseType.H2)
                .addScript("sql/schema.sql")
                .addScript("sql/giftCertificate-data.sql")
                .build();

        jdbcTemplate = new JdbcTemplate(database);
        repository = new GiftCertificateRepository(jdbcTemplate);
    }

    @AfterEach
    public void tearDown() {
        database.shutdown();
    }
    @Test
    void testGetAllMethod() {
        List<GiftCertificate> certificateList = repository.getAll();

//       check for null
        assertNotNull(certificateList);
//
        assertEquals(3, certificateList.size());

        assertEquals("certificate one", certificateList.get(0).getName());
        assertEquals("certificate two", certificateList.get(1).getName());
    }

    @Test
    void testGetById() {
        GiftCertificate actualCertificate = repository.getById(1);
        assertNotNull(actualCertificate);
        assertEquals("certificate one", actualCertificate.getName());


//        check for non-existing certificate
        GiftCertificate foundCertificate = repository.getById(99);

        // Then
        assertNull(foundCertificate);
    }

    @Test
    void testByTagId() {
//        load tags into in memory database
        jdbcTemplate.execute("INSERT INTO tags(name) VALUES ('tag one')");
        jdbcTemplate.execute("INSERT INTO tags(name) VALUES ('tag two')");
        //        load GiftCertificateTags into in memory database
        jdbcTemplate.execute("INSERT INTO gift_certificate_tags(gift_certificate_id, tag_id) VALUES (1, 2)");
        jdbcTemplate.execute("INSERT INTO gift_certificate_tags(gift_certificate_id, tag_id) VALUES (2, 2)");

        List<GiftCertificate> certificateList = repository.getByTagId(2);

        assertEquals(2, certificateList.size());

        assertEquals("certificate one", certificateList.get(0).getName());

    }

    @Test
    void testGetByTagIdNotFound() {
//        check by non-existing tag ID
        List<GiftCertificate> certificatesWithTag = repository.getByTagId(99);

        assertTrue(certificatesWithTag.isEmpty());
    }

    @Test
    void testFindByPartialName() {
        // input name
        String partialName = "certificate";

        List<GiftCertificate> certificatesWithPartialName = repository.findByPartialName(partialName);

        // Then
        assertNotNull(certificatesWithPartialName);
        assertEquals(2, certificatesWithPartialName.size());

        // comparing names
        assertEquals("certificate one", certificatesWithPartialName.get(0).getName());
        assertEquals("certificate two", certificatesWithPartialName.get(1).getName());
    }

    @Test
    void tesSaveMethod() {
        GiftCertificate certificateToSave = new GiftCertificate();
        certificateToSave.setName("new certificate");
        certificateToSave.setDescription("description");
        certificateToSave.setPrice(3000);
        certificateToSave.setDuration(50);
        certificateToSave.setCreateDate(LocalDateTime.of(2023, 10, 1, 11, 11, 11));
        certificateToSave.setLastUpdateDate(LocalDateTime.of(2023, 12, 1, 11, 11, 11));

        GiftCertificate expectedCertificate = repository.save(certificateToSave);

        assertNotNull(expectedCertificate);

//        comparing fields
        assertEquals(4, expectedCertificate.getId());
        assertEquals(certificateToSave.getName(), expectedCertificate.getName());
        assertEquals(certificateToSave.getDescription(), expectedCertificate.getDescription());
        assertEquals(certificateToSave.getDuration(), expectedCertificate.getDuration());
        assertEquals(certificateToSave.getPrice(), expectedCertificate.getPrice());

    }

    @Test
    void testUpdateMethod() {
//        setting value into giftCertificate fields
        GiftCertificate certificateToSave = new GiftCertificate();
        certificateToSave.setId(1);
        certificateToSave.setName("new certificate");
        certificateToSave.setDescription("description");
        certificateToSave.setPrice(2000);
        certificateToSave.setDuration(30);
        certificateToSave.setCreateDate(LocalDateTime.of(2022, 10, 1, 11, 11, 11));
        certificateToSave.setLastUpdateDate(LocalDateTime.of(2023, 12, 1, 11, 11, 11));

        GiftCertificate expectedCertificate = repository.update(certificateToSave);

        assertNotNull(expectedCertificate);

        assertEquals(1, expectedCertificate.getId());
        assertEquals(certificateToSave.getName(), expectedCertificate.getName());
        assertEquals(certificateToSave.getDescription(), expectedCertificate.getDescription());
        assertEquals(certificateToSave.getDuration(), expectedCertificate.getDuration());
        assertEquals(certificateToSave.getPrice(), expectedCertificate.getPrice());
    }

    @Test
    void testDelete() {
        Integer certificateIdToDelete = 1;

        boolean deletionResult = repository.delete(certificateIdToDelete);

        assertTrue(deletionResult);

        // Verify that the certificate is no longer in the database
        GiftCertificate retrievedCertificate = repository.getById(certificateIdToDelete);
        assertNull(retrievedCertificate);
    }

    @Test
    void testDeleteNotFound() {
        boolean deletionResult = repository.delete(99);

        assertFalse(deletionResult);
    }


}
