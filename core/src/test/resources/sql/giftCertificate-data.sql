INSERT INTO gift_certificates(name, description, price, duration, create_date, last_update_date)
values ('certificate one', 'gives promotion for position', 100, 30, '2023-11-29T06:12:15.156',
        '2023-09-29T06:12:15.156');
INSERT INTO gift_certificates(name, description, price, duration, create_date, last_update_date)
values ('certificate two', 'gives price more', 200, 60, '2023-11-29T06:12:15.156', '2023-12-29T06:12:15.156');
INSERT INTO gift_certificates(name, description, price, duration, create_date, last_update_date)
values ('description for name', 'example', 1000, 60, '2023-08-29T06:12:15.156', '2023-08-29T06:12:15.156');