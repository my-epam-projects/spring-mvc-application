drop table if exists gift_certificates;
drop table if exists tags;
drop table if exists gift_certificate_tags;

create table gift_certificates
(
    id               serial primary key not null,
    name             varchar(255)       not null,
    description      varchar            not null,
    price            integer            not null,
    duration         integer            not null,
    create_date      TIMESTAMP(3),
    last_update_date TIMESTAMP(3)
);

create table tags
(
    id   serial primary key,
    name varchar(255) not null
);

create table gift_certificate_tags
(
    gift_certificate_id integer references gift_certificates (id) not null,
    tag_id              integer references tags (id)              not null
);
