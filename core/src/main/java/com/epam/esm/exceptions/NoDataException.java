package com.epam.esm.exceptions;

public class NoDataException extends Exception{
    public NoDataException(String message) {
        super(message);
    }
}
