package com.epam.esm.exceptions;

public class DatabaseErrorException extends Exception{

    public DatabaseErrorException(String message) {
        super(message);
    }
}
