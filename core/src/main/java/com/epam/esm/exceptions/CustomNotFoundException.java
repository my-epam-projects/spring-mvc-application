package com.epam.esm.exceptions;

public class CustomNotFoundException extends Exception{

    public CustomNotFoundException(String message) {
        super(message);
    }
}
