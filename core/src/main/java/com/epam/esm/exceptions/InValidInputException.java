package com.epam.esm.exceptions;

public class InValidInputException extends Exception{

    public InValidInputException(String message) {
        super(message);
    }
}
