package com.epam.esm.exceptions;


import com.epam.esm.common.ResponseData;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class GlobalExceptionHandling extends ResponseEntityExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<?> handleGenericException(final Exception e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler(RuntimeException.class)
    public ResponseEntity<?> handleRuntimeException(final RuntimeException e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.BAD_GATEWAY);
    }

    @ExceptionHandler(NoDataException.class)
    public ResponseEntity<?> handleNoDataException(final NoDataException e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

        @ExceptionHandler(CustomNotFoundException.class)
    public ResponseEntity<?> handleNotFoundException(final CustomNotFoundException e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InValidInputException.class)
    public ResponseEntity<?> handleInValidInputException(final InValidInputException e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(AlreadyExistsException.class)
    public ResponseEntity<?> handleAlreadyExistsException(final AlreadyExistsException e) {
        return new ResponseEntity<>(new ResponseData<>(e.getMessage()), HttpStatus.BAD_REQUEST);
    }


}
