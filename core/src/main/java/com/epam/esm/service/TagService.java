package com.epam.esm.service;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.exceptions.NoDataException;

import java.util.List;

public interface TagService {
    List<TagDTO> getAll() throws CustomNotFoundException;

    TagDTO getById(Integer id) throws NoDataException, InValidInputException, CustomNotFoundException;

    TagDTO create(TagCreateDTO dto) throws NoDataException, AlreadyExistsException;

    TagDTO update(Integer id, TagUpdateDTO dto) throws NoDataException, AlreadyExistsException, InValidInputException, CustomNotFoundException;

    Boolean delete(Integer id) throws NoDataException, InValidInputException, CustomNotFoundException;

    Tag findByName(String name);

    List<TagDTO> getByGiftCertificate(Integer certificateId) throws CustomNotFoundException;

    boolean findByIds(List<Integer> tagIds);
}
