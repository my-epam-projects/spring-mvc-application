package com.epam.esm.service;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.DatabaseErrorException;
import com.epam.esm.exceptions.InValidInputException;

import java.util.List;

public interface GiftCertificateService {
    List<GiftCertificateDTO> getAll() throws CustomNotFoundException;

    GiftCertificateDTO getById(Integer id) throws CustomNotFoundException;

    GiftCertificateDTO create(GiftCertificateCreateDTO dto) throws InValidInputException, CustomNotFoundException, DatabaseErrorException;

    GiftCertificateDTO update(Integer id,GiftCertificateUpdateDTO dto) throws InValidInputException, CustomNotFoundException;

    Boolean delete(Integer id) throws CustomNotFoundException, DatabaseErrorException;

    List<GiftCertificateDTO> getByTagName(String name) throws CustomNotFoundException;

    List<GiftCertificateDTO> getByName(String name) throws InValidInputException, CustomNotFoundException;

    Boolean addNewTag(GiftCertificateTagDTO dto) throws InValidInputException, CustomNotFoundException, DatabaseErrorException;

    Boolean deleteTag(GiftCertificateTagDTO dto) throws CustomNotFoundException, InValidInputException, DatabaseErrorException;

    List<GiftCertificateDTO> getByDesc(String desc) throws InValidInputException, CustomNotFoundException;
}
