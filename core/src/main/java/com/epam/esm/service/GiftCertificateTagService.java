package com.epam.esm.service;

import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;

public interface GiftCertificateTagService {

    boolean save(GiftCertificateTagDTO dto);

    boolean delete(GiftCertificateTagDTO dto);

    boolean deleteByCertificate(Integer certificateId);
}
