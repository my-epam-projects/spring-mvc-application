package com.epam.esm.service.impl;

import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.entity.GiftCertificateTags;
import com.epam.esm.repository.GiftCertificateTagRepository;
import com.epam.esm.service.GiftCertificateTagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GiftCertificateTagServiceImpl implements GiftCertificateTagService {

    private final GiftCertificateTagRepository repository;

    @Autowired
    public GiftCertificateTagServiceImpl(GiftCertificateTagRepository repository) {
        this.repository = repository;
    }


    @Transactional
    public boolean save(GiftCertificateTagDTO dto) {
        return dto.getTagIds().stream()
                .allMatch(tagId -> repository.save(new GiftCertificateTags(dto.getCertificateId(), tagId)));
    }

    @Transactional
    public boolean delete(GiftCertificateTagDTO dto) {
        return dto.getTagIds().stream()
                .allMatch(tagId -> repository.delete(new GiftCertificateTags(dto.getCertificateId(), tagId)));
    }

    @Transactional
    public boolean deleteByCertificate(Integer certificateId) {
        return repository.deleteByCertificate(certificateId);
    }
}

