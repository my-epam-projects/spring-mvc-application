package com.epam.esm.service.impl;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.exceptions.NoDataException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.TagMapper;
import com.epam.esm.repository.TagRepository;
import com.epam.esm.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class TagServiceImpl implements TagService {

    private static final Logger LOGGER = LogManager.getLogger(TagServiceImpl.class);
    private final TagRepository repository;

    @Autowired
    public TagServiceImpl(TagRepository repository) {
        this.repository = repository;

    }

    @Override
    public List<TagDTO> getAll() throws CustomNotFoundException {
        List<TagDTO> dtoList = TagMapper.toTagDtoList(repository.getAll());
        if (Utils.isEmpty(dtoList)) {
            LOGGER.warn("tags are not found");
            throw new CustomNotFoundException("Tags are not found");
        }
        return dtoList;
    }


    @Override
    public TagDTO getById(Integer id) throws NoDataException, InValidInputException, CustomNotFoundException {
        //        validating ID
        this.validateId(id);
        return TagMapper.toDto(this.findById(id));
    }

    //    for saving data into database returns true if data is saved successfully

    @Transactional
    @Override
    public TagDTO create(TagCreateDTO dto) throws NoDataException, AlreadyExistsException {
//        checks tag name for not blank
        this.validateName(dto.getName());

        //        checking tag name
        this.checkTagNotExists(dto.getName());

        Tag tag = TagMapper.toEntity(dto);
        return TagMapper.toDto(repository.save(tag));
    }

    // for updating data returns true if data is updated successfully
    @Transactional
    @Override
    public TagDTO update(Integer id,TagUpdateDTO dto) throws NoDataException, AlreadyExistsException, InValidInputException, CustomNotFoundException {
        //        validating ID
        this.validateId(id);
//        checking tag name for isValid
        this.validateName(dto.getName());

        this.findById(id);
//        checking tag name
        this.checkTagNotExists(dto.getName());

        Tag tag = TagMapper.toEntity(dto);
        return TagMapper.toDto(repository.update(tag));
    }

    @Transactional
    @Override
    public Boolean delete(Integer id) throws NoDataException, InValidInputException, CustomNotFoundException {
//        validating ID
        this.validateId(id);
        if (Utils.isPresent(this.findById(id))) {
            return repository.delete(id);
        } else {
            throw new CustomNotFoundException("Tag Id " + id + " is not found");
        }
    }

    private void validateId(Integer id) throws NoDataException, InValidInputException {
        if (id == null) {
            LOGGER.warn("tag id is required");
            throw new NoDataException("Tag id is required");
        } else if (id == 0) {
            LOGGER.warn("zero value is not acceptable");
            throw new InValidInputException("zero is not acceptable");
        }
    }

    private void validateName(String name) throws NoDataException {
        if (Utils.isEmpty(name)) {
            LOGGER.warn("tag name cannot be empty");
            throw new NoDataException("Tag name cannot be empty");
        }
    }

    private void checkTagNotExists(String name) throws AlreadyExistsException {
        if (Utils.isPresent(repository.getByName(name))) {
            LOGGER.warn("Tag name {} is already exists", name);
            throw new AlreadyExistsException("Tag name " + name + " is already exists");
        }
    }

    public List<TagDTO> getByGiftCertificate(Integer certificateId) throws CustomNotFoundException {
        List<Tag> tagList = repository.getByCertificate(certificateId);
        if (Utils.isEmpty(tagList)) {
            LOGGER.warn("tags are not found for Gift certificate ID {} ", certificateId);
            throw new CustomNotFoundException("tags are not found for Gift certificate ID: " + certificateId);
        }
        return TagMapper.toTagDtoList(tagList);
    }

    public Tag findByName(String name) {
        return repository.getByName(name);
    }


    private Tag findById(Integer id) throws CustomNotFoundException {
        Tag tag = repository.findById(id);
        if (Utils.isEmpty(tag)) {
            LOGGER.warn("Tag with Id: {} is not found", id);
            throw new CustomNotFoundException("tag with Id: " + id + " is not found");
        }
        return tag;
    }

    public boolean findByIds(List<Integer> tagIds) {
        return tagIds.stream().allMatch(tagId -> {
            try {
                return Utils.isPresent(this.findById(tagId));
            } catch (CustomNotFoundException e) {
                return false;
            }
        });
    }
}
