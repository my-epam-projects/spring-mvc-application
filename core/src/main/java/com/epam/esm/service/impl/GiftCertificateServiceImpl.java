package com.epam.esm.service.impl;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.entity.Tag;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.DatabaseErrorException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.helper.Utils;
import com.epam.esm.mapper.GiftCertificateMapper;
import com.epam.esm.repository.GiftCertificateRepository;
import com.epam.esm.service.GiftCertificateService;
import com.epam.esm.service.GiftCertificateTagService;
import com.epam.esm.service.TagService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.function.Supplier;


@Service
public class GiftCertificateServiceImpl implements GiftCertificateService {

    private static final Logger LOGGER = LogManager.getLogger(GiftCertificateServiceImpl.class);
    private final GiftCertificateRepository repository;
    private final TagService tagService;
    private final GiftCertificateTagService giftCertificateTagService;


    @Autowired
    public GiftCertificateServiceImpl(GiftCertificateRepository repository, TagService tagService, GiftCertificateTagService giftCertificateTagService) {
        this.repository = repository;
        this.tagService = tagService;
        this.giftCertificateTagService = giftCertificateTagService;

    }

    @Override
    public List<GiftCertificateDTO> getAll() throws CustomNotFoundException {
        List<GiftCertificate> certificateList = repository.getAll();
        if (Utils.isEmpty(certificateList)) {
            LOGGER.warn("gift certificates are not found");
            throw new CustomNotFoundException("gift certificates are not found");
        }

        return this.mapCertificatesToDTOs(certificateList);
    }

    @Override
    public GiftCertificateDTO getById(Integer id) throws CustomNotFoundException {
        GiftCertificate giftCertificate = this.findById(id);
        GiftCertificateDTO dto = GiftCertificateMapper.toDto(giftCertificate);
        List<TagDTO> tagDTOList = tagService.getByGiftCertificate(id);
        dto.setTagDTOList(tagDTOList);
        return dto;
    }

    @Override
    public List<GiftCertificateDTO> getByTagName(String name) throws CustomNotFoundException {
        Tag tag = tagService.findByName(name);
        if (Utils.isEmpty(tag)) {
            LOGGER.warn("tag {} is not found", name);
            throw new CustomNotFoundException("tag name: " + name + " is not found");
        }
        List<GiftCertificate> certificateList = repository.getByTagId(tag.getId());
        if (certificateList.isEmpty()) {
            throw new CustomNotFoundException("gift certificates are not found for corresponding tag Name: " + name);
        }
        return mapCertificatesToDTOs(certificateList);
    }

    @Override
    public List<GiftCertificateDTO> getByName(String name) throws InValidInputException, CustomNotFoundException {
        if (Utils.isEmpty(name)) {
            LOGGER.warn("input name is empty");
            throw new InValidInputException("input name is empty");
        }
        List<GiftCertificate> certificateList = repository.findByPartialName(name);
        if (Utils.isEmpty(certificateList)) {
            throw new CustomNotFoundException("Gift certificates are not found for given name: " + name);
        }
        return mapCertificatesToDTOs(certificateList);
    }

    @Override
    public List<GiftCertificateDTO> getByDesc(String desc) throws InValidInputException, CustomNotFoundException {
        if (Utils.isEmpty(desc)) {
            LOGGER.warn("input value is empty");
            throw new InValidInputException("input value is empty");
        }
        List<GiftCertificate> certificateList = repository.findByPartialDesc(desc);
        if (Utils.isEmpty(certificateList)) {
            throw new CustomNotFoundException("Gift certificates are not found for given value: " + desc);
        }
        return mapCertificatesToDTOs(certificateList);
    }

    @Transactional
    @Override
    public GiftCertificateDTO create(GiftCertificateCreateDTO dto) throws InValidInputException, CustomNotFoundException, DatabaseErrorException {
        if (Utils.isEmpty(dto)) {
            LOGGER.warn("request model is null!!!");
            throw new InValidInputException("request model is empty");
        }
//        validating input tag Ids
        if (!tagService.findByIds(dto.getTagIds())) {
            throw new CustomNotFoundException("Invalid tag input values");
        }

//         mapping from entity to giftCertificateDto
        GiftCertificate certificate = GiftCertificateMapper.toEntity(dto);
//        saving new certificate and mapping it into dto
        GiftCertificateDTO certificateDTO = GiftCertificateMapper.toDto(repository.save(certificate));

        GiftCertificateTagDTO certificateTagDTO = new GiftCertificateTagDTO(certificateDTO.getId(), dto.getTagIds());

//        saving tags and giftCertificate into giftCertificateTags entity
        handleDatabaseError(() -> giftCertificateTagService.save(certificateTagDTO), "Unwanted error occurred while adding a new tag");

        List<TagDTO> tagDTOList = tagService.getByGiftCertificate(certificateDTO.getId());
        certificateDTO.setTagDTOList(tagDTOList);
        return certificateDTO;
    }

    @Transactional
    @Override
    public GiftCertificateDTO update(Integer id, GiftCertificateUpdateDTO dto) throws InValidInputException, CustomNotFoundException {
        if (Utils.isEmpty(dto)) {
            LOGGER.warn("request model is null!!!");
            throw new InValidInputException("request model is empty");
        }
        if (Utils.isEmpty(findById(id))) {
            throw new CustomNotFoundException("giftCertificate id: " + id);
        }
        GiftCertificate certificate = GiftCertificateMapper.toEntity(dto);
        certificate.setId(id);
        GiftCertificateDTO certificateDTO = GiftCertificateMapper.toDto(repository.update(certificate));
        List<TagDTO> tagDTOList = tagService.getByGiftCertificate(certificateDTO.getId());
        certificateDTO.setTagDTOList(tagDTOList);
        return certificateDTO;
    }

    @Transactional
    @Override
    public Boolean delete(Integer id) throws CustomNotFoundException, DatabaseErrorException {
        this.findById(id);
        handleDatabaseError(() -> giftCertificateTagService.deleteByCertificate(id), "Unwanted error occurred while deleting tags");
        return repository.delete(id);
    }


    @Override
    public Boolean addNewTag(GiftCertificateTagDTO dto) throws InValidInputException, CustomNotFoundException, DatabaseErrorException {
        validateAndHandleErrors(dto);
        return handleDatabaseError(() -> giftCertificateTagService.save(dto), "Unwanted error occurred while adding a new tag");
    }

    @Override
    public Boolean deleteTag(GiftCertificateTagDTO dto) throws CustomNotFoundException, InValidInputException, DatabaseErrorException {
        validateAndHandleErrors(dto);
        return handleDatabaseError(() -> giftCertificateTagService.delete(dto), "Unwanted error occurred while deleting the tag");
    }

    //    handles database errors
    private <T> T handleDatabaseError(Supplier<T> databaseOperation, String errorMessage) throws DatabaseErrorException {
        try {
            return databaseOperation.get();
        } catch (Exception e) {
            LOGGER.warn(errorMessage, e);
            throw new DatabaseErrorException(errorMessage);
        }
    }


    //    finds tags for corresponding certificate and maps them into DTO object
    private List<GiftCertificateDTO> mapCertificatesToDTOs(List<GiftCertificate> certificateList) {
        return certificateList.stream()
                .map(certificate -> {
                    GiftCertificateDTO dto = GiftCertificateMapper.toDto(certificate);
                    try {
                        List<TagDTO> tagDTOList = tagService.getByGiftCertificate(dto.getId());
                        dto.setTagDTOList(tagDTOList);
                    } catch (CustomNotFoundException e) {
                        throw new RuntimeException(e.getMessage());
                    }
                    return dto;
                })
                .toList();
    }

    public void validateAndHandleErrors(GiftCertificateTagDTO dto) throws InValidInputException, CustomNotFoundException {
        if (Utils.isEmpty(dto)) {
            LOGGER.warn("Request model cannot be null!!!");
            throw new InValidInputException("Request model cannot be null!!");
        }

        this.findById(dto.getCertificateId());

        boolean isValid = tagService.findByIds(dto.getTagIds());
        if (!isValid) {
            throw new CustomNotFoundException("Invalid tag input values");
        }
    }

    public GiftCertificate findById(Integer id) throws CustomNotFoundException {
        GiftCertificate certificate = repository.getById(id);
        if (Utils.isEmpty(certificate)) {
            LOGGER.warn("Gift certificate with Id: {} is not found", id);
            throw new CustomNotFoundException("Gift certificate with Id: " + id + " is not found");
        }
        return certificate;
    }

}
