package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
//todo component vd repository
@Repository
public class GiftCertificateRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public GiftCertificateRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<GiftCertificate> getAll() {
        final String query = "SELECT * FROM gift_certificates ORDER BY create_date DESC,name";
        return jdbcTemplate.query(query, this.giftCertificateRowMapper());
    }

    public GiftCertificate getById(Integer id) {
        final String query = "SELECT * FROM gift_certificates WHERE id = ?";

        try {
            return jdbcTemplate.queryForObject(query, this.giftCertificateRowMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<GiftCertificate> getByTagId(Integer tagId) {
        final String query = "SELECT * FROM gift_certificates WHERE id IN (SELECT gift_certificate_id FROM gift_certificate_tags WHERE tag_id = ?) ORDER BY create_date DESC,name";
        try {
            return jdbcTemplate.query(query, this.giftCertificateRowMapper(), tagId);
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        }
    }

    public List<GiftCertificate> findByPartialName(String name) {
        final String query = "SELECT * FROM gift_certificates WHERE name LIKE ? ORDER BY create_date DESC,name";
        String likePattern = "%" + name + "%";

        try {
            return jdbcTemplate.query(query, this.giftCertificateRowMapper(), likePattern);
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        }
    }

    public List<GiftCertificate> findByPartialDesc(String desc) {
        final String query = "SELECT * FROM gift_certificates WHERE description LIKE ?";
        String likePattern = "%" + desc + "%";
        return jdbcTemplate.query(query, this.giftCertificateRowMapper(), likePattern);
    }

    public GiftCertificate save(GiftCertificate certificate) {
        final String query = "insert into gift_certificates(name, description, price, duration, create_date, last_update_date)" +
                "values (?,?,?,?,?,?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, certificate.getName());
            ps.setString(2, certificate.getDescription());
            ps.setInt(3, certificate.getPrice());
            ps.setInt(4, certificate.getDuration());
            ps.setTimestamp(5, Timestamp.valueOf(certificate.getCreateDate()));
            ps.setTimestamp(6, Timestamp.valueOf(certificate.getLastUpdateDate()));
            return ps;
        }, keyHolder);

        int certificateId = (Integer) keyHolder.getKeyList().get(0).get("id");

        return this.getById(Math.toIntExact(certificateId));
    }

    public GiftCertificate update(GiftCertificate certificate) {
        final String query = "UPDATE gift_certificates SET name = ?,description = ?,price = ?,duration = ?,create_date = ?,last_update_date = ? WHERE id = ?";
        Object[] params = new Object[]{certificate.getName(), certificate.getDescription(), certificate.getPrice(),
                certificate.getDuration(), certificate.getCreateDate(), certificate.getLastUpdateDate(), certificate.getId()};
        return jdbcTemplate.update(query, params) > 0 ? this.getById(certificate.getId()) : null;
    }

    public boolean delete(Integer id) {
        final String query = "DELETE FROM gift_certificates WHERE id = ?";
        return jdbcTemplate.update(query, id) > 0;
    }

    private RowMapper<GiftCertificate> giftCertificateRowMapper() {
        return (rs, rowNum)
                -> {
            GiftCertificate certificate = new GiftCertificate();
            certificate.setId(rs.getInt("id"));
            certificate.setName(rs.getString("name"));
            certificate.setDescription(rs.getString("description"));
            certificate.setPrice(rs.getInt("price"));
            certificate.setDuration(rs.getInt("duration"));
            certificate.setCreateDate(rs.getTimestamp("create_date").toLocalDateTime());
            certificate.setLastUpdateDate(rs.getTimestamp("last_update_date").toLocalDateTime());
            return certificate;
        };
    }
}
