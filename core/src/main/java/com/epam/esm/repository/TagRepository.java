package com.epam.esm.repository;

import com.epam.esm.entity.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TagRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public TagRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public List<Tag> getAll() {
        final String query = "select * from tags";

        return jdbcTemplate.query(query, (resultSet, rowNum) -> {
            Tag tag = new Tag();
            tag.setId(resultSet.getInt("id"));
            tag.setName(resultSet.getString("name"));
            return tag;
        });
    }

    public Tag findById(Integer id) {
        final String query = "select * from tags where id = ?";

        RowMapper<Tag> tagRowMapper = (rs, rowNum)
                -> {
            Tag tag = new Tag();
            tag.setId(rs.getInt("id"));
            tag.setName(rs.getString("name"));
            return tag;
        };
        try {
            return jdbcTemplate.queryForObject(query, tagRowMapper, id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public Tag save(Tag tag) {
        final String query = "INSERT INTO tags(name) VALUES (?)";

        KeyHolder keyHolder = new GeneratedKeyHolder();

        jdbcTemplate.update(connection -> {
            PreparedStatement ps = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            ps.setString(1, tag.getName());
            return ps;
        }, keyHolder);

        int tagId = (Integer) keyHolder.getKeyList().get(0).get("id");
        return this.findById(tagId);
    }

    public Tag update(Tag tag) {
        final String query = "UPDATE tags SET name = ? WHERE id = ?";
        return jdbcTemplate.update(query, tag.getName(), tag.getId()) > 0 ? this.findById(tag.getId()) : null;
    }

    public boolean delete(Integer id) {
        final String query = "DELETE FROM tags WHERE id = ?";
        return jdbcTemplate.update(query, id) > 0;
    }

    public Tag getByName(String name) {
        final String query = "SELECT * FROM tags WHERE name = ?";

        RowMapper<Tag> tagRowMapper = (rs, rowNum)
                -> {
            Tag tag = new Tag();
            tag.setId(rs.getInt("id"));
            tag.setName(rs.getString("name"));
            return tag;
        };
        try {
            return jdbcTemplate.queryForObject(query, tagRowMapper, name);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public List<Tag> getByCertificate(Integer certificateId) {
        final String query = "SELECT  * FROM tags WHERE id IN (SELECT tag_id FROM gift_certificate_tags WHERE gift_certificate_id = ?)";
        RowMapper<Tag> tagRowMapper = (rs, rowNum)
                -> {
            Tag tag = new Tag();
            tag.setId(rs.getInt("id"));
            tag.setName(rs.getString("name"));
            return tag;
        };
        try {
            return jdbcTemplate.query(query, tagRowMapper, certificateId);
        } catch (EmptyResultDataAccessException e) {
            return new ArrayList<>();
        }
    }
}

