package com.epam.esm.repository;

import com.epam.esm.entity.GiftCertificateTags;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

@Repository
public class GiftCertificateTagRepository {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public GiftCertificateTagRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    public boolean save(GiftCertificateTags certificateTags) {
        final String query = "INSERT INTO gift_certificate_tags VALUES (?,?)";
        Object[] params = new Object[]{certificateTags.getGitCertificateId(), certificateTags.getTagId()};
        return jdbcTemplate.update(query, params) > 0;
    }

    public boolean delete(GiftCertificateTags certificateTags) {
        final String query = "DELETE FROM gift_certificate_tags WHERE tag_id = ? AND gift_certificate_id = ?";
        Object[] params = new Object[]{certificateTags.getTagId(), certificateTags.getGitCertificateId(),};
        return jdbcTemplate.update(query, params) > 0;
    }

    public boolean deleteByCertificate(Integer certificateId) {
        final String query = "DELETE FROM gift_certificate_tags WHERE gift_certificate_id = ?";
        return jdbcTemplate.update(query, certificateId) > 0;
    }


}
