package com.epam.esm.mapper;

import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.entity.GiftCertificate;
import com.epam.esm.helper.Utils;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public class GiftCertificateMapper {


    private GiftCertificateMapper() {

    }

    public static GiftCertificate toEntity(GiftCertificateCreateDTO dto) {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setName(dto.getName());
        certificate.setDescription(dto.getDescription());
        certificate.setPrice(dto.getPrice());
        certificate.setDuration(dto.getDuration());
        certificate.setCreateDate(Utils.toLocalDateTime(dto.getCreateDate()));
        certificate.setLastUpdateDate(Utils.toLocalDateTime(dto.getLastUpdateDate()));
        return certificate;
    }

    public static GiftCertificate toEntity(GiftCertificateUpdateDTO dto) {
        GiftCertificate certificate = new GiftCertificate();
        certificate.setName(dto.getName());
        certificate.setDescription(dto.getDescription());
        certificate.setPrice(dto.getPrice());
        certificate.setDuration(dto.getDuration());
        certificate.setCreateDate(Utils.toLocalDateTime(dto.getCreateDate()));
        certificate.setLastUpdateDate(Utils.toLocalDateTime(dto.getLastUpdateDate()));
        return certificate;
    }

    public static GiftCertificateDTO toDto(GiftCertificate certificate) {
        GiftCertificateDTO dto = new GiftCertificateDTO();
        dto.setId(certificate.getId());
        dto.setName(certificate.getName());
        dto.setDescription(certificate.getDescription());
        dto.setPrice(certificate.getPrice());
        dto.setDuration(certificate.getDuration());
        dto.setCreateDate(String.valueOf(certificate.getCreateDate()));
        dto.setLastUpdateDate(String.valueOf(certificate.getLastUpdateDate()));
        return dto;
    }

    public static List<GiftCertificateDTO> toDtoList(List<GiftCertificate> certificateList) {
        return certificateList.stream()
                .map(GiftCertificateMapper::toDto)
                .toList();
    }
}
