package com.epam.esm.mapper;

import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.entity.Tag;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class TagMapper {

    private TagMapper() {

    }

    public static Tag toEntity(TagCreateDTO dto) {
        Tag tag = new Tag();
        tag.setName(dto.getName());
        return tag;
    }

    public static Tag toEntity(TagUpdateDTO dto) {
        Tag tag = new Tag();
        tag.setName(dto.getName());
        return tag;
    }

    public static TagDTO toDto(Tag tag) {
        return new TagDTO(tag.getId(), tag.getName());
    }

    public static List<TagDTO> toTagDtoList(List<Tag> tagList) {
        return tagList.stream()
                .map(TagMapper::toDto)
                .toList();
    }

}
