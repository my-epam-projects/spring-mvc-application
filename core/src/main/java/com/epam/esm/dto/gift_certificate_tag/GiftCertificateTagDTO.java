package com.epam.esm.dto.gift_certificate_tag;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateTagDTO {

    private Integer certificateId;

    private List<Integer> tagIds;
}
