package com.epam.esm.dto.gift_certificate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateUpdateDTO {

    private String name;

    private String description;

    private Integer price;

    private Integer duration;

    private String createDate;

    private String lastUpdateDate;

}
