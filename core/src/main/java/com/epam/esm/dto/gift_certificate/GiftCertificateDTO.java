package com.epam.esm.dto.gift_certificate;

import com.epam.esm.dto.tag.TagDTO;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateDTO {

    private Integer id;

    private String name;

    private String description;

    private Integer price;

    private Integer duration;

    private String createDate;

    private String lastUpdateDate;

    private List<TagDTO> tagDTOList;
}
