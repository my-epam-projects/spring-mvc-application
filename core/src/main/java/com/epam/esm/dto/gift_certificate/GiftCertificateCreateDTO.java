package com.epam.esm.dto.gift_certificate;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class GiftCertificateCreateDTO {


    private String name;

    private String description;

    private Integer price;

    private Integer duration;

    private String createDate;

    private String lastUpdateDate;

    private List<Integer> tagIds;
}
