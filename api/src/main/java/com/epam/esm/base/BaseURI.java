package com.epam.esm.base;

public interface BaseURI {

    String GIFT_CERTIFICATES = "/gift_certificates";
    String TAGS = "/tags";

    String CREATE = "/create";
    String UPDATE = "/update";
    String GET = "/get";
    String ALL = "/all";
    String DELETE = "/delete";
    String BY_TAG = "/tag";
    String BY_NAME = "/name";
    String BY_DESC = "/description";

}
