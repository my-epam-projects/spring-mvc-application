package com.epam.esm.controller;

import com.epam.esm.base.BaseURI;
import com.epam.esm.common.ResponseData;
import com.epam.esm.dto.tag.TagCreateDTO;
import com.epam.esm.dto.tag.TagDTO;
import com.epam.esm.dto.tag.TagUpdateDTO;
import com.epam.esm.exceptions.AlreadyExistsException;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.exceptions.NoDataException;
import com.epam.esm.service.TagService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BaseURI.TAGS)
public class TagController {

    private final TagService tagService;

    @Autowired
    public TagController(TagService tagService) {
        this.tagService = tagService;
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<TagDTO>>> getAll() throws CustomNotFoundException {
        return ResponseData.success200(tagService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<TagDTO>> getById(@PathVariable(value = "id") Integer id) throws CustomNotFoundException, NoDataException, InValidInputException {
        return ResponseData.success200(tagService.getById(id));
    }

    @PostMapping
    public ResponseEntity<ResponseData<TagDTO>> create(@RequestBody TagCreateDTO dto) throws NoDataException, AlreadyExistsException {
        return ResponseData.success201(tagService.create(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<TagDTO>> update(@PathVariable(value = "id") Integer id,
                                                       @RequestBody TagUpdateDTO dto) throws AlreadyExistsException, NoDataException, InValidInputException, CustomNotFoundException {
        return ResponseData.success202(tagService.update(id, dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<ResponseData<Boolean>> delete(@PathVariable(value = "id") Integer id) throws NoDataException, InValidInputException, CustomNotFoundException {
        return ResponseData.success204(tagService.delete(id));
    }
}
