package com.epam.esm.controller;

import com.epam.esm.base.BaseURI;
import com.epam.esm.common.ResponseData;
import com.epam.esm.dto.gift_certificate.GiftCertificateCreateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateDTO;
import com.epam.esm.dto.gift_certificate.GiftCertificateUpdateDTO;
import com.epam.esm.dto.gift_certificate_tag.GiftCertificateTagDTO;
import com.epam.esm.exceptions.CustomNotFoundException;
import com.epam.esm.exceptions.DatabaseErrorException;
import com.epam.esm.exceptions.InValidInputException;
import com.epam.esm.service.GiftCertificateService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(BaseURI.GIFT_CERTIFICATES)
public class GiftCertificateController {

    private final GiftCertificateService giftCertificateService;

    @Autowired
    public GiftCertificateController(GiftCertificateService giftCertificateService) {
        this.giftCertificateService = giftCertificateService;
    }

    @GetMapping
    public ResponseEntity<ResponseData<List<GiftCertificateDTO>>> getAll() throws CustomNotFoundException {
        return ResponseData.success200(giftCertificateService.getAll());
    }

    @GetMapping("/{id}")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> getById(@PathVariable(value = "id") Integer id) throws CustomNotFoundException {
        return ResponseData.success200(giftCertificateService.getById(id));
    }

    @GetMapping(BaseURI.BY_TAG)
    public ResponseEntity<ResponseData<List<GiftCertificateDTO>>> getByTagName(@RequestParam(value = "tag_name") String name) throws CustomNotFoundException {
        return ResponseData.success200(giftCertificateService.getByTagName(name));
    }

    @GetMapping(BaseURI.BY_NAME)
    public ResponseEntity<ResponseData<List<GiftCertificateDTO>>> getByName(@RequestParam(value = "name") String name) throws CustomNotFoundException, InValidInputException {
        return ResponseData.success200(giftCertificateService.getByName(name));
    }

    @GetMapping(BaseURI.BY_DESC)
    public ResponseEntity<ResponseData<List<GiftCertificateDTO>>> getByDesc(@RequestParam(value = "description") String desc) throws CustomNotFoundException, InValidInputException {
        return ResponseData.success200(giftCertificateService.getByDesc(desc));
    }

    @PostMapping(BaseURI.CREATE)
    public ResponseEntity<ResponseData<GiftCertificateDTO>> create(@RequestBody GiftCertificateCreateDTO dto) throws InValidInputException, CustomNotFoundException, DatabaseErrorException {
        return ResponseData.success201(giftCertificateService.create(dto));
    }

    @PutMapping("/{id}")
    public ResponseEntity<ResponseData<GiftCertificateDTO>> update(@PathVariable(value = "id") Integer id,
                                                                   @RequestBody GiftCertificateUpdateDTO dto) throws InValidInputException, CustomNotFoundException {
        return ResponseData.success202(giftCertificateService.update(id, dto));
    }

    @DeleteMapping
    public ResponseEntity<ResponseData<Boolean>> delete(@RequestParam(value = "id") Integer id) throws CustomNotFoundException, DatabaseErrorException {
        return ResponseData.success204(giftCertificateService.delete(id));
    }


    @PostMapping
    public ResponseEntity<ResponseData<Boolean>> addNewTag(@RequestBody GiftCertificateTagDTO dto) throws CustomNotFoundException, InValidInputException, DatabaseErrorException {
        return ResponseData.success201(giftCertificateService.addNewTag(dto));
    }

    @DeleteMapping
    public ResponseEntity<ResponseData<Boolean>> deleteTag(@RequestBody GiftCertificateTagDTO dto) throws CustomNotFoundException, DatabaseErrorException, InValidInputException {
        return ResponseData.success204(giftCertificateService.deleteTag(dto));
    }
}
